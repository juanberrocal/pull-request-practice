export class Human {
  private weight: number;
  private height: number;
  private width: number;

  constructor(weight: number, height: number, width: number) {
    this.weight = weight;
    this.height = height;
    this.width = width;
  }

  getHumanInfo() {
    return {
      weight: this.weight,
      height: this.height,
      width: this.width
    };
  }
}
