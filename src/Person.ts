import { Human } from './Human';
export class Person extends Human {
  private dni: string;
  private name: string;
  private age: number;
  private job: string;

  constructor(
    weight: number,
    height: number,
    width: number,
    dni: string,
    name: string,
    age: number,
    job: string
  ) {
    super(weight, height, width);
    this.dni = dni;
    this.name = name;
    this.age = age;
    this.job = job;
  }

  getPersonInfo() {
    return {
      dni: this.dni,
      name: this.name,
      age: this.age,
      job: this.job
    };
  }
}
