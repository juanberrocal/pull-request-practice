export class Animal {
  private name: string;
  private owner: string;

  constructor(name: string, owner: string) {
    this.name = name;
    this.owner = owner;
  }

  getAnimalInfo() {
    return {
      name: this.name,
      owner: this.owner
    };
  }
}
