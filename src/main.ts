import { Person } from './Person';
import { Animal } from './Animal';

const person1 = new Person(55, 120, 90, '671892789910', 'Luis', 15, 'student');
const animal = new Animal('Bob', 'Luis');
console.log(person1.getPersonInfo());
console.log(animal.getAnimalInfo());
